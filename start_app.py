'''
Created on 20 nov. 2019

entry point for the tool

@author: eduardo.grana@gmail.com
'''
import os

from Qt import QtWidgets  # @UnresolvedImport
import main_interface


def main():
    ''' runs the whole circus '''

    # get an app
    app, already_running = getApp()

    # get a parent (or not) for the ui
    parent = getParent()

    # create the ui with it's parent and show it
    ui = main_interface.MainUI(parent)
    ui.show()

    # if the app was not running, run it!
    if not already_running:
        app.exec_()


def getParent():
    ''' tryes to find a parent for this ui
    Args:
        None
    Returns:
        QWidget or None: the qwidget for parenting or None if not suitable parent
            was found
    '''
    # use a dict to add more apps later
    parent_map = {'maya': _getMayaMainWindow,
                  }

    # look for apps in each item
    for name, fn in parent_map.items():  # @UnusedVariable
        parent = fn()
        if parent:
            return parent

    # no valid widget found :(
    return None


def getApp():
    ''' Gets a runninf q application to avoid creating another one
    Args:
        None
    Returns:
        tuple(QApplication, bool): the existing or created QApp and wheter or
            not this app is already executing
    '''
    app = QtWidgets.QApplication.instance()
    if app:
        already_running = True
    else:
        app = QtWidgets.QApplication([])
        already_running = False
    return app, already_running


# helpers ---

def _getMayaMainWindow():
    ''' Tries to get the maya main window
    Args:
        None
    Returns:
        QWidget or None: the maya main window or None if it could not be found
    '''

    if not os.getenv('MAYA_LOCATION'):
        return None

    try:
        # we need to wrap a pointer as a pyside widget so we need help!
        from shiboken2 import wrapInstance
        from maya import OpenMayaUI as omui

        # get opinter object
        ptr = omui.MQtUtil.mainWindow()

        # cast pointer to widget (may fail)
        main_window = wrapInstance(long(ptr), QtWidgets.QWidget)

    except Exception, e:  # @UnusedVariable
        main_window = None

    return main_window


if __name__ == '__main__':
    main()
