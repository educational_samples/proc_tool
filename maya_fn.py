'''
Created on 23 nov. 2019

some silly maya functions for testing

@author: eduardo.grana@gmail.com
'''
import os
from maya import cmds


def exportStuff():
    ''' utility to export selection form a function '''
    return MayaStuffExporter.exportSelectedStuff()


def importStuff():
    ''' utility to import exported stuff form a function '''
    return MayaStuffExporter.importStuff()


class MayaStuffExporter(object):
    ''' helper class for maya functions'''
    EXPORT_FILE = r'{}\Desktop\auto_exp.ma'.format(os.getenv('HOME', r'C:\temp'))

    @classmethod
    def exportSelectedStuff(cls):
        ''' exports selected objects in maya to an arbitrary ma file'''

        sel_list = cmds.ls(sl=True)
        if not sel_list:
            cmds.confirmDialog(message='Select Something!')
            return

        path = cls.EXPORT_FILE
        cmds.file(path, es=True, type="mayaAscii", f=1)

    @classmethod
    def importStuff(cls):
        ''' import objects into maya from an arbitrary ma file'''

        path = cls.EXPORT_FILE
        cmds.file(path, i=True, dns=True)
