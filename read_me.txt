eduardo.grana@gmail.com

This is a sill example of a procedural tool that load commads. It's intended just as
a learning tool and not a real production thing

topics to discuss
- using qt4/qt5 with Qt wrapper
- running from os and maya
    - new app vs already exiting app
    - finding parent
- creating widget from ui with loaduitype
- adding widgets to list items
- class properties
- classmethods
- connecting signals
- message box statics
- glob
- subprocess
- imp
- traceback
- config files
- reload
- bat files


# connect to eclipse debugger
import sys
eclipse_path = r'C:\Users\eduardo\eclipse\javascript-oxygen\eclipse\dropins\plugins'
pydevd_path = eclipse_path + r'\org.python.pydev_6.2.0.201711281614\pysrc'
sys.path.append(pydevd_path)
import pydevd  # @UnresolvedImport
pydevd.settrace(stdoutToServer=True, stderrToServer=True, suspend=False)
print 'connected to pydev server?'

# maya start up
path = r'D:\work\eclipse_workspace'  # <- path to where the tool is
import sys
sys.path.append(path)
from proc_tool import start_app 
start_app.main()
