'''
Created on 20 nov. 2019

This is the main interface

@author: eduardo.grana@gmail.com
'''
import os

from Qt import QtWidgets, QtGui, QtCore  # @UnresolvedImport

import loadUiType
import tool_item


# create base class from ui file
MAIN_UI_FILE_REL_PATH = 'ui/main.ui'
MAIN_UI_FULL_PATH = os.path.join(os.path.dirname(__file__), MAIN_UI_FILE_REL_PATH)
MAIN_FORM_CLASS, MAIN_BASE_CLASS = loadUiType.loadUiType(MAIN_UI_FULL_PATH)


class MainUI(MAIN_FORM_CLASS, MAIN_BASE_CLASS):
    ''' This class implements the main ui '''

    def __init__(self, parent=None):

        # configure parent class and apply ui definition
        super(MainUI, self).__init__(parent=parent)
        self.setupUi(self)

        # populate everything and connect signals
        self.reload()
        self.wireUI()

    def reload(self):
        ''' populates contexts and tools '''
        self.populateContexts()
        self.populateTools()

    def wireUI(self):
        ''' connect signals so widgets work '''
        self.reload_pushButton.clicked.connect(self.reload)
        self.context_comboBox.currentIndexChanged.connect(self.populateTools)

    @property
    def current_context(self):
        ''' return the current context name as a str '''
        text = self.context_comboBox.currentText()
        if not text:
            return ''
        return text

    def populateContexts(self):
        ''' adds all contexts to the combo box '''
        self.context_comboBox.clear()
        context_list = self._getContexts()
        self.context_comboBox.addItems(context_list)

    def populateTools(self):
        ''' adds all the tools for current context '''
        self.listWidget.clear()
        functions = self._getFunctions()
        for fn_def in functions:
            if self.current_context not in fn_def.get('env', []):
                continue
            self.addFileToList(fn_def)

    def _getFunctions(self):
        ''' returns all the function data from config file (function_def.py)
        Args:
            None
        Returns:
            list[dict]: al list of function definition
        '''
        import function_def
        reload(function_def)
        return function_def.FUNCIONS

    def _getContexts(self):
        ''' returns all the contexts from config file (function_def.py)
        Args:
            None
        Returns:
            list[str]: al list of context names
        '''
        functions = self._getFunctions()
        all_envs = []
        for fn in functions:
            env_list = fn.get('env') or []
            for env in env_list:
                if env in all_envs:
                    continue
                all_envs.append(env)
        all_envs.sort()
        return all_envs

    def addFileToList(self, fn_def):
        ''' Adds a tool widget to the list based on function definition
        Args:
            fn_def (dict): the function definition
        Returns:
            None
        '''

        # create an item for the list. this will hold the custom widget later
        item = QtWidgets.QListWidgetItem()
        item.setSizeHint(QtCore.QSize(tool_item.WIDTH, tool_item.HEIGHT))

        # create the widget and add cross reference the widget and the item
        widget = tool_item.ToolItem(self, fn_def)
        item.tool_widget = widget
        widget.list_item = item

        # add the item to the list and assign the widget to the item
        self.listWidget.addItem(item)
        self.listWidget.setItemWidget(item, widget)

    def contextMenuEvent(self, event):  # @UnusedVariable
        ''' Shows a context menu when requested by a right mouse button click
        Args:
            event (Qt event): the context menu event to handle. This argument is not used right now
        Returns:
            None
        '''

        # create the menu
        menu = QtWidgets.QMenu(self)

        # add sample command
        this_action = QtWidgets.QAction('Test', self)
        this_action.triggered.connect(self.about)
        menu.addAction(this_action)

        menu.popup(QtGui.QCursor.pos())

    def setStatus(self, message):
        ''' sets a message at the bottom of the ui
        Args:
            message (str): the message to display
        Returns:
            None
        '''
        self.status_label.setText(message)

    def about(self):
        ''' sample function to use in the sample popup menu'''
        QtWidgets.QMessageBox.about(self, 'About!', 'This is very useful')
