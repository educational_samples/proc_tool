'''
Created on 17 nov. 2019

@author: eduardo
'''
import xml.etree.ElementTree as xml
from cStringIO import StringIO


def findModule(module):
    ''' check to see if a module exists
    Args:
        module (str): the name of the module to check
    Returns:
        bool: whether the module is available for import or not
    '''
    import imp
    try:
        imp.find_module(module)
        return True
    except ImportError:
        return False


# this a quick fix for getting this runnin in maya with pyside2
# without breaking the other pyside 1 tools
if findModule('PySide'):
    import PySide
    import pysideuic
    class_cmd = 'PySide.QtGui.%s'
else:
    import PySide2 as PySide  # @UnresolvedImport
    import pyside2uic as pysideuic  # @UnresolvedImport
    # from PySide2 import QtUiTools
#     import PySide2 as Qt
    class_cmd = 'PySide.QtWidgets.%s'


def loadUiType(uiFile):
    """
    Pyside lacks the "loadUiType" command, so we have to convert the ui file to py code in-memory first
    and then execute it in a special frame to retrieve the form_class.
    """
    parsed = xml.parse(uiFile)
    widget_class = parsed.find('widget').get('class')
    form_class = parsed.find('class').text

    with open(uiFile, 'r') as f:
        o = StringIO()
        frame = {}

        pysideuic.compileUi(f, o, indent=0)
        pyc = compile(o.getvalue(), '<string>', 'exec')
        exec pyc in frame

        # Fetch the base_class and form class based on their type in the xml
        # from designer
        form_class = frame['Ui_%s' % form_class]
        base_class = eval(class_cmd % widget_class)

    return form_class, base_class
