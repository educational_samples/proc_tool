'''
Created on 20 nov. 2019

Here we define all the functions to be loaded by the ui

@author: eduardo.grana@gmail.com
'''

# environments
os_env = 'os'
maya_all_env = 'maya_all'
maya_rig_env = 'maya_rig'

# open maya fn
open_maya_main = {'module': 'os_fn', 'function': 'launchMaya', 'args': [], 'kwargs': {}}
open_maya_opt = {}
open_maya_dev = {'email': 'eduardo.grana@gmail.com'}
open_maya_fn = {'env': [os_env], 'icon': 'maya.jpg', 'name': 'Open Maya',
                'main': open_maya_main, 'opt': open_maya_opt, 'dev': open_maya_dev,
                'tooltip': 'Open maya! (should there be more explanation?)'}

# opendesigner
open_designer_main = {'module': 'os_fn', 'function': 'launchDesigner', 'args': [], 'kwargs': {}}
open_designer_opt = {}
open_designer_dev = {}
open_designer_fn = {'env': [os_env], 'icon': 'qt.png', 'name': 'Open Designer',
                    'main': open_designer_main, 'opt': open_designer_opt, 'dev': open_designer_dev}

# export selected to temp
exp_temp_main = {'module': 'maya_fn', 'function': 'exportStuff', 'args': [], 'kwargs': {}}
exp_temp_opt = {}
exp_temp_dev = {}
exp_temp_fn = {'env': [maya_all_env], 'icon': 'export.png', 'name': 'Export Selected to Temp',
               'main': exp_temp_main, 'opt': exp_temp_opt, 'dev': exp_temp_dev}

# import selected from temp
imp_temp_main = {'module': 'maya_fn', 'function': 'importStuff', 'args': [], 'kwargs': {}}
imp_temp_opt = {}
imp_temp_dev = {}
imp_temp_fn = {'env': [maya_all_env], 'icon': 'import.png', 'name': 'Import from Temp',
               'main': imp_temp_main, 'opt': imp_temp_opt, 'dev': imp_temp_dev}

# all functions here!
FUNCIONS = [open_maya_fn, open_designer_fn, exp_temp_fn, imp_temp_fn]
