'''
Created on 23 nov. 2019

some silly os functions for testing

@author: eduardo.grana@gmail.com
'''
import glob
import subprocess


def launchMaya():
    ''' utility to launch maya form a function '''
    return Launcher.maya()


def launchMaya2019():
    ''' utility to launch maya 2019 form a function '''
    return Launcher.maya2019()


def launchDesigner():
    ''' utility to launch designer form a function '''
    ''' utility to launch designer form a function '''
    return Launcher.designer()


class Launcher(object):
    ''' class to handle launching of programs '''

    @classmethod
    def maya(cls):
        ''' launch maya '''
        glob_path = r"C:\Program Files*\Autodesk\Maya*\bin\maya.exe"
        name = 'Maya'
        return cls._launch(glob_path, name)

    @classmethod
    def maya2019(cls):
        ''' launch maya 2019 '''
        glob_path = r"C:\Program Files*\Autodesk\Maya2019*\bin\maya.exe"
        name = 'Maya 2019'
        return cls._launch(glob_path, name)

    @classmethod
    def designer(cls):
        ''' launch designer '''
        glob_path = r"C:\Program Files*\Autodesk\Maya*\bin\designer.exe"
        name = 'Qt Designer'
        return cls._launch(glob_path, name)

    @classmethod
    def _launch(cls, glob_path, name):
        ''' launch an executable
        Args:
            glob_path (str): the glob path to find the executable
            name (str): the name of the app for logging purposes
        Returns:
            None
        '''
        found = glob.glob(glob_path)
        if not found:
            raise Exception('Could not find {} path with glob {}'.format(name, glob_path))
        found.sort()
        path = found[-1]
        cmd = '"{}"'.format(path)
        subprocess.Popen(cmd)
