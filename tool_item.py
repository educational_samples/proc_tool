'''
Created on 20 nov. 2019

This is the tool item

@author: eduardo.grana@gmail.com
'''
import os
import imp
import traceback

from Qt import QtWidgets, QtGui, QtCore  # @UnresolvedImport
import loadUiType
import proc_tool_utils

# create base class from ui file
MAIN_UI_FILE_REL_PATH = 'ui/tool_item.ui'
MAIN_UI_FULL_PATH = os.path.join(os.path.dirname(__file__), MAIN_UI_FILE_REL_PATH)
MAIN_FORM_CLASS, MAIN_BASE_CLASS = loadUiType.loadUiType(MAIN_UI_FULL_PATH)

WIDTH = 200
HEIGHT = 80


class ToolItem(MAIN_FORM_CLASS, MAIN_BASE_CLASS):
    ''' This class implements a tool item '''

    def __init__(self, parent_ui, function_def):
        # configure parent class and apply ui definition
        super(ToolItem, self).__init__()
        self.setupUi(self)

        # save argumets for later
        self.parent_ui = parent_ui
        self.function_def = function_def

        # initialize widgets
        self.setIcon()
        self.setName()
        self.setToolTip()

        # set action buttons enabled based on command existence
        self.go_pushButton.setEnabled(bool(self.main_fn))
        self.options_pushButton.setEnabled(bool(self.opt_fn))
        self.blame_pushButton.setEnabled(bool(self.dev_fn))

        # connect signals
        self.go_pushButton.clicked.connect(self.runMain)
        self.options_pushButton.clicked.connect(self.runOptional)
        self.blame_pushButton.clicked.connect(self.runDeveloper)

    # tool tip ---
    @property
    def tool_tip(self):
        ''' return tool tip as a str or None if not defined'''
        return self.function_def.get('tooltip')

    def setToolTip(self):
        ''' sets tool tip on label '''
        if not self.tool_tip:
            return
        self.icon_label.setToolTip(str(self.tool_tip))

    # name ---
    @property
    def tool_name(self):
        ''' return tool name as a str or None if not defined'''
        return self.function_def.get('name')

    def setName(self):
        ''' sets tool name on button '''
        self.go_pushButton.setText(self.tool_name)

    # name ---
    @property
    def icon(self):
        ''' get the icon a s afull path
        Args:
            None
        Returns:
            str or None: full path to the icon or None if not defined
        '''
        # check icon is defined
        icon = self.function_def.get('icon')
        if not icon:
            return None

        # convert to full path
        ui_folder = proc_tool_utils.getUiFolder()
        return os.path.join(ui_folder, icon)

    def setIcon(self):
        ''' sets the icon in the label ui '''

        # check if icon is defined, otherwise exit
        icon = self.icon
        if not icon:
            return

        # create icon and scale it to fit
        pixmap = QtGui.QPixmap(icon)
        pixmap = pixmap.scaledToHeight(HEIGHT - 10, QtCore.Qt.SmoothTransformation)

        # set the icon and align to center
        self.icon_label.setPixmap(pixmap)
        self.icon_label.setAlignment(QtCore.Qt.AlignCenter)

    # functions ---

    @property
    def main_fn(self):
        ''' return main function data as a dict or None if not defined'''
        return self.function_def.get('main')

    def runMain(self):
        ''' run "main" function '''
        return self._prompt_run(self.main_fn)

    @property
    def opt_fn(self):
        ''' return optional function data as a dict or None if not defined'''
        return self.function_def.get('opt')

    def runOptional(self):
        ''' run "options" function '''
        return self._prompt_run(self.opt_fn)

    @property
    def dev_fn(self):
        ''' return dev function data as a dict or None if not defined'''
        return self.function_def.get('dev')

    def runDeveloper(self):
        ''' run "developer" function '''
        return self._prompt_run(self.dev_fn)

    def _prompt_run(self, fn_data):
        ''' run a command and catch exceptions with an error prompt
        Args:
            fn_data (dict): the command description
        Returns:
            None
        '''
        try:
            self._run(fn_data)
        except Exception, e:  # @UnusedVariable
            self.promptError()

    def _run(self, fn_data):
        ''' run a command
        Args:
            fn_data (dict): the command description
        Returns:
            object: whatever the command returns
        '''
        data = {}
        for key in ['module', 'function', 'args', 'kwargs']:
            value = fn_data.get(key)
            if value is None:
                raise Exception('Cant find {} definition on function'.format(key))
            data[key] = value

        # import module
        module_name = data['module']
        fp, pathname, description = imp.find_module(module_name)
        module = imp.load_module(module_name, fp, pathname, description)
        reload(module)

        function = getattr(module, data['function'])

        return function(*data['args'], **data['kwargs'])

    def promptError(self):
        ''' prompt the trace if an exception happened '''
        error_message = 'There was an error. Dont panic and try to understand the trace below:'
        error_message += '\n\n{}'.format(traceback.format_exc())
        QtWidgets.QMessageBox.warning(self, 'Not great news', error_message)
