'''
Created on 23 nov. 2019

@author: eduardo.grana@gmail.com
'''
import os


def getUiFolder():
    ''' gets the full path for ui folder
    Args:
        None
    Returns:
        str: the full path to the ui folder
    '''
    dirname = os.path.dirname(__file__)  # this is a bit hacky
    return os.path.join(dirname, 'ui')
